package com.example.liam.guessthenumber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    int targetNumber = (int)(Math.random() * 100 + 1);
    int guesses = 0;
    int timeElapsed = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                timeElapsed = timeElapsed + 1;
            }
        }, 0, 1000);

        final Button submit = findViewById(R.id.submit);
        final TextView message = findViewById(R.id.message);
        final EditText guess = findViewById(R.id.guess);
        final Button reset = findViewById(R.id.reset);

        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                guesses = guesses + 1;
                // Code here executes on main thread after user presses button
                int guessNumber = Integer.parseInt(guess.getText().toString());
                if(guessNumber == targetNumber) {
                    message.setText("Congrats, you won!\nYou guessed the number correctly in "+guesses+" guesses!\nYour time was "+timeElapsed+" seconds!");
                    submit.setVisibility(View.GONE);
                    guess.setVisibility(View.GONE);
                    reset.setVisibility(View.VISIBLE);
                }else if(guessNumber < targetNumber){
                    message.setText("Try a higher number...");
                }else {
                    message.setText("Try a lower number...");
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                targetNumber = (int)(Math.random() * 100 + 1);
                guesses = 0;
                timeElapsed = 0;
                message.setText("Type a guess to start");
                guess.setText("");
                submit.setVisibility(View.VISIBLE);
                guess.setVisibility(View.VISIBLE);
                reset.setVisibility(View.GONE);
            }
        });

    }
}
